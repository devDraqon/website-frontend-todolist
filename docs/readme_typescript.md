<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article> 
    <header> 
        <h1> Boilerplate Docs - Typescript </h1>
        <section>
            <h3> What is Typescript? </h3>
            <p> 
                TypeScript is a typed superset of JavaScript that compiles to plain JavaScript. It offers classes, modules, and interfaces to help you build robust and strongtyped components.
            </p>
        </section>
    </header>
    <main>
    </main>
    <aside>
        <section>
            <h3> Learn more about Typescript </h3>
            <a class="bold" href="https://www.typescriptlang.org/docs/handbook/typescript-from-scratch.html"> Click here </a> to visit Typescripts Website.
        </section>
    </aside>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./readme.md"> Click here </a> to get back to the Documentation Overview.
        </p>
    </footer>
</article>