<head>
    <link rel="stylesheet" href="./docs.css">
</head>
<article> 
    <header>
        <h1> Boilerplate Docs - Npm </h1>
        <section>
            <h3> What is Npm? </h3>
            <p> 
                npm stands for Node Package Manager is a package manager for JavaScript which allows developers to publish own packages and to install  packages other developers published. 
            </p>
        </section>
    </header>
    <aside>
        <h3> Requirements </h3>
        <p> node and npm must be installed locally on your machine.
        </p>
    </aside>
    <main>
        <section> 
            <table>
                <h3> General Commands </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> npm install </td>
                    <td> Install / update all dependencies & dev-dependencies for this project. </td>
                </tr>
                <tr>
                    <td class="bold"> npm install -g npm </td>
                    <td> Updates npm to its latest stable version. </td>
                </tr>
            </table>
        </section>
        <section> 
            <table>
                <h3> Available Scripts </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> npm run start </td>
                    <td> Starts the script with the name 'start' which in this case starts the development server for this project.</td>
                </tr>
                <tr>
                    <td class="bold"> npm run build </td>
                    <td> Starts the script with the name 'build' which in this case builds the <b>src</b> folder into a production ready application, which can be located inside the <b>dist</b> folder.</td>
                </tr>
            </table>
            <p><i> 
                <b>hint:</b>
                <br/> 
                Since the start script is used so widely by many developers, its possible to write 'npm start' instead of 'npm run start'.
            </i></p>
        </section>
        <section>
            <table class="col3">
                <h3> Install specific package version </h3>
                <tr>
                    <th> Command </th>
                    <th> Package Version </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name </td>
                    <td class="bold"> latest stable </td>
                    <td> Install / update latest stable version of package </td>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name@latest </td>
                    <td class="bold"> latest stable </td>
                    <td> Install / update latest stable version of package </td>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name@next </td>
                    <td class="bold"> latest alpha </td>
                    <td> Install / update latest alpha version of package </td>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name@1.0.0 </td>
                    <td class="bold"> version 1.0.0 </td>
                    <td> Install / update version 1.0.0 of package </td>
                </tr>
            </table>
            <p><i> 
                <b>hint:</b>
                <br/> 
                You may only target other versions than @latest if you run into compability issues with a specific dependency. 
            </i></p>
        </section>
        <section>
            <table>
                <h3> Install package as dependency or globally </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name </td>
                    <td> install package locally. Package can be used in this project, but will be forgotten after checking out to another branch.</td>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name --save </td>
                    <td> install package as depdenceny for this project. Package can be used in this project, when working in development mode and in production mode. </td>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name --save-dev </td>
                    <td> install package as dev-dependency for this project. Package can be used in this project, but may only work in development mode.</td>
                </tr>
                <tr>
                    <td class="bold"> npm install package_name -g </td>
                    <td> install package globally on your machine. Package can be used everywhere, but only on your machine. </td>
                </tr>
            </table>
            <p><i> 
                <b>hint:</b>
                <br/> 
                When installing dependencies for your project, its considered a best-practise if you install them with --save. 
            </i></p>
        </section>
        <section>
            <table class="col3">
                <h3> Remove specific package </h3>
                <tr>
                    <th> Command </th>
                    <th> Description </th>
                </tr>
                <tr>
                    <td class="bold"> npm remove package_name </td>
                    <td> removes package from dependencies / dev-dependencies. Package and dependency are removed from this <b>project</b>. </td>
                </tr>
                <tr>
                    <td class="bold"> npm -g uninstall package_name </td>
                    <td> removes package from dependencies / dev-dependencies. Package is removed from this <b>machine</b>. </td>
                </tr>
            </table>
            <p><i> 
                <b>hint:</b>
                <br/> 
                npm uninstall and npm remove actually do the same. 
            </i></p>
        </section>
        <section>
            <h2> Your Project dependencies </h2>
            <p>
                Every package installed via --save or --save-dev can be found in the package.json file for this project as dependency or dev-dependency. 
                Dependencies which are listed in the package.json file, will be installed via 'npm install' automaticially and can be used throughout the project once they are installed.
            </p>
            <p>
                To view the full list of dependencies, visit the package.json file at the root of this project.
            </p>
        </section>
    </main>
    <aside>
        <section>
            <h3> Learn more about Npm </h3>
            <a class="bold" href="https://docs.npmjs.com/about-npm"> Click here </a> to visit Npms Website.
        </section>
    </aside>
    <hr>
    <footer>
        <h3> Documentation Overview </h3>
        <p> 
            <a class="bold" href="./readme.md"> Click here </a> to get back to the Documentation Overview.
        </p>
    </footer>
</article>
